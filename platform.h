#ifndef SEPARATE_PLATFORM_H
#define SEPARATE_PLATFORM_H

#ifdef __GNUC__
#define ATTR_PACKED_STRUCT __attribute__ ((gcc_struct, packed))
#else
#define ATTR_PACKED_STRUCT
#endif

#ifdef G_OS_WIN32

#define DEFAULT_RGB_PROFILE g_build_filename( g_getenv( "SYSTEMROOT" ), "\\system32\\spool\\drivers\\color\\sRGB Color Space Profile.icm", NULL )
#define DEFAULT_CMYK_PROFILE g_build_filename( g_getenv( "SYSTEMROOT" ), "\\system32\\spool\\drivers\\color\\USWebCoatedSWOP.icc", NULL )
/* For Japanese users */
//#define DEFAULT_CMYK_PROFILE g_build_filename( g_getenv( "SYSTEMROOT" ), "\\system32\\spool\\drivers\\color\\JapanColor2001Coated.icc", NULL )

#else

#ifndef O_BINARY

#define O_BINARY 0

#endif /* O_BINARY */

#ifdef __APPLE__ 

#define DEFAULT_RGB_PROFILE g_strdup( "/System/Library/ColorSync/Profiles/sRGB Profile.icc" )
#define DEFAULT_CMYK_PROFILE g_strdup( "/System/Library/ColorSync/Profiles/Generic CMYK Profile.icc" )

#else

#define DEFAULT_RGB_PROFILE g_strdup( "/usr/share/color/icc/sRGB Color Space Profile.icm" )
#define DEFAULT_CMYK_PROFILE g_strdup( "/usr/share/color/icc/USWebCoatedSWOP.icc" )

#endif /* __APPLE__ */

#endif /* G_OS_WIN32 */

#if ! (GIMP_MAJOR_VERSION > 2 || (GIMP_MAJOR_VERSION == 2 && GIMP_MINOR_VERSION >= 4))
#define USE_ICC_BUTTON
#endif

#if ! GIMP_CHECK_VERSION(2, 8, 0)
#define gimp_image_attach_parasite gimp_image_parasite_attach
#define gimp_image_detach_parasite gimp_image_parasite_detach
#define gimp_image_get_parasite gimp_image_parasite_find
#define gimp_image_insert_layer(i, l, p, po) (gimp_image_add_layer (i, l, po))
#define gimp_image_insert_channel(i, c, p, po) (gimp_image_add_channel (i, c, po))
#define gimp_image_insert_vectors(i, v, p, po) (gimp_image_add_vectors (i, v, po))
#define gimp_image_select_item(i, o, t) (gimp_selection_load (t))
#else
#define gimp_vectors_get_name gimp_item_get_name
#define gimp_drawable_get_name gimp_item_get_name
#define gimp_drawable_get_image gimp_item_get_image
#define gimp_drawable_get_visible gimp_item_get_visible
#define gimp_drawable_set_visible gimp_item_set_visible
#define gimp_vectors_get_visible gimp_item_get_visible
#define gimp_vectors_set_visible gimp_item_set_visible
#define gimp_vectors_get_linked gimp_item_get_linked
#define gimp_vectors_set_linked gimp_item_set_linked
#endif

#endif
