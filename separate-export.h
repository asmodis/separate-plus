/* separate+ 0.5 - image processing plug-in for the Gimp
 *
 * Copyright (C) 2002-2004 Alastair Robinson (blackfive@fakenhamweb.co.uk),
 * Based on code by Andrew Kieschnick and Peter Kirchgessner
 * 2007-2010 Modified by Yoshinori Yamakawa (yamma-ma@users.sourceforge.jp)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef SEPARATE_EXPORT_H
#define SEPARATE_EXPORT_H

// The user specified filename for direct export
#define EXPORT_FILENAME "separate_export_filename"
// In the export dialog a user indicates that the filename has been specified
// When this parasite does not exist at the time of "reapplying" of the export, a dialog is displayed
// Let's specify filename
#define EXPORT_FILENAME_SPECIFIED "separate_export_filename_specified"

typedef gboolean (*SeparateExportFunc) (gchar *, gint32, gconstpointer, gsize, gconstpointer, gsize, gboolean);

void separate_export (GimpDrawable    *drawable,
                      SeparateContext *sc);

#endif
